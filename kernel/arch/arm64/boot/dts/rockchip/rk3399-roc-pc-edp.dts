/*
 * Copyright (c) 2018 Zhongshan Rockchip Electronics Co., Ltd
 *
 */
/dts-v1/;

#include "dt-bindings/pwm/pwm.h"
#include <dt-bindings/input/input.h>
//#include <dt-bindings/display/drm_mode.h>
#include "rk3399.dtsi"
#include "rk3399-opp.dtsi"
#include "rk3399-android.dtsi"
//#include "rk3399-firefly-demo.dtsi"

/ {
	model = "roc-rk3399-pc-edp (Android)";
	compatible = "rockchip,roc-rk3399-pc", "rockchip,rk3399";

	edp_panel: edp-panel {
		compatible = "simple-panel";
		bus-format = <MEDIA_BUS_FMT_RGB666_1X18>;

		backlight = <&backlight>;

		display-timings {
			native-mode = <&timing0>;

			timing0: timing0 {
				clock-frequency = <200000000>;
				hactive = <2560>;
				vactive = <1600>;
				hfront-porch = <48>;
				hsync-len = <80>;
				hback-porch = <32>;
				vfront-porch = <2>;
				vsync-len = <39>;
				vback-porch = <5>;
				hsync-active = <0>;
				vsync-active = <0>;
				de-active = <0>;
				pixelclk-active = <0>;
			};
		};

		ports {
			panel_in_edp: endpoint {
				remote-endpoint = <&edp_out_panel>;
			};
		};

		power_ctr: power_ctr {
               rockchip,debug = <1>;
               lcd_en: lcd-en {
                       gpios = <&gpio2 5 GPIO_ACTIVE_HIGH>;
					   pinctrl-names = "default";
					   pinctrl-0 = <&lcd_panel_enable>;
                       rockchip,delay = <1000>;
               };
			   /*
               lcd_edp_hpd: lcd-edp-hpd {
                       gpios = <&gpio1 20 GPIO_ACTIVE_HIGH>;
					   pinctrl-names = "default";
					   pinctrl-0 = <&lcd_edp_hpd_enable>;
                       rockchip,delay = <10>;
               };
			   */
       };
	};

	backlight: backlight {
			status = "okay";
			compatible = "pwm-backlight";
			pwms = <&pwm0 0 25000 0>;
			enable-gpios = <&gpio1 24 GPIO_ACTIVE_HIGH>;
			pinctrl-names = "default";
			pinctrl-0 = <&lcd_backlight_enable>;
			brightness-levels = <
			  0   1   2   3   4   5   6   7
			  8   9  10  11  12  13  14  15
			 16  17  18  19  20  21  22  23
			 24  25  26  27  28  29  30  31
			 32  33  34  35  36  37  38  39
			 40  41  42  43  44  45  46  47
			 48  49  50  51  52  53  54  55
			 56  57  58  59  60  61  62  63
			 64  65  66  67  68  69  70  71
			 72  73  74  75  76  77  78  79
			 80  81  82  83  84  85  86  87
			 88  89  90  91  92  93  94  95
			 96  97  98  99 100 101 102 103
			104 105 106 107 108 109 110 111
			112 113 114 115 116 117 118 119
			120 121 122 123 124 125 126 127
			128 129 130 131 132 133 134 135
			136 137 138 139 140 141 142 143
			144 145 146 147 148 149 150 151
			152 153 154 155 156 157 158 159
			160 161 162 163 164 165 166 167
			168 169 170 171 172 173 174 175
			176 177 178 179 180 181 182 183
			184 185 186 187 188 189 190 191
			192 193 194 195 196 197 198 199
			200 201 202 203 204 205 206 207
			208 209 210 211 212 213 214 215
			216 217 218 219 220 221 222 223
			224 225 226 227 228 229 230 231
			232 233 234 235 236 237 238 239
			240 241 242 243 244 245 246 247
			248 249 250 251 252 253 254 255>;
		default-brightness-level = <200>;
	};

	test-power {
		status = "okay";
	};

	clkin_gmac: external-gmac-clock {
		compatible = "fixed-clock";
		clock-frequency = <125000000>;
		clock-output-names = "clkin_gmac";
		#clock-cells = <0>;
	};

	dw_hdmi_audio: dw-hdmi-audio {
		status = "disabled";
		compatible = "rockchip,dw-hdmi-audio";
		#sound-dai-cells = <0>;
	};

	hdmi_sound: hdmi-sound {
		status = "disabled";
		compatible = "simple-audio-card";
		simple-audio-card,format = "i2s";
		simple-audio-card,mclk-fs = <256>;
		simple-audio-card,name = "rockchip,hdmi";

		simple-audio-card,cpu {
			sound-dai = <&i2s2>;
		};
		simple-audio-card,codec {
			sound-dai = <&hdmi>;
		};
	};

	rockchip-key {
		compatible = "rockchip,key";
		status = "okay";

		io-channels = <&saradc 1>;
		recovery-key {
			linux,code = <113>;
			label = "F12";
			rockchip,adc_value = <4>;
		};
	};

    leds {
       compatible = "gpio-leds";
       power_led: power {
           label = "firefly:blue:power";
           linux,default-trigger = "ir-power-click";
           default-state = "on";
           gpios = <&gpio2 27 GPIO_ACTIVE_HIGH>;
           pinctrl-names = "default";
           pinctrl-0 = <&led_power>;
       };
       user_led: user {
           label = "firefly:yellow:user";
           linux,default-trigger = "ir-user-click";
           default-state = "off";
           gpios = <&gpio0 13 GPIO_ACTIVE_HIGH>;
           pinctrl-names = "default";
           pinctrl-0 = <&led_user>;
       };
   };


	vcc3v3_sys: vcc3v3-sys {
		compatible = "regulator-fixed";
		regulator-name = "vcc3v3_sys";
		regulator-always-on;
		regulator-boot-on;
		regulator-min-microvolt = <3300000>;
		regulator-max-microvolt = <3300000>;
	};

	vcc5v0_host: vcc5v0-host-regulator {
		compatible = "regulator-fixed";
		enable-active-high;
		gpio = <&gpio1 0 GPIO_ACTIVE_HIGH>;
		pinctrl-names = "default";
		pinctrl-0 = <&host_vbus_drv>;
		regulator-name = "vcc5v0_host";
		regulator-always-on;
	};

	vcc_hub_en: vcc-hub-en-regulator {
		compatible = "regulator-fixed";
		enable-active-high;
		gpio = <&gpio2 4 GPIO_ACTIVE_HIGH>;
		pinctrl-names = "default";
		pinctrl-0 = <&hub_rst_en>;
		regulator-name = "vcc_hub_en";
		regulator-always-on;
	};

	vcc_sd: vcc-sd {
		compatible = "regulator-fixed";
		enable-active-high;
		gpio = <&gpio4 30 GPIO_ACTIVE_HIGH>;
		pinctrl-names = "default";
		pinctrl-0 = <&vcc_sd_h>;
		regulator-name = "vcc_sd";
		regulator-min-microvolt = <3300000>;
		regulator-max-microvolt = <3300000>;
		//regulator-always-on;
		//regulator-boot-on;
	};

	vcc5v0_sys: vcc5v0-sys {
		status = "okay";
		compatible = "regulator-fixed";
		regulator-name = "vcc5v0_sys";
//		enable-active-high;
//		gpio = <&gpio2 6 GPIO_ACTIVE_HIGH>;
//		pinctrl-names = "default";
//		pinctrl-0 = <&vcc_5v0_h>;
		regulator-always-on;
		regulator-boot-on;
		regulator-min-microvolt = <5000000>;
		regulator-max-microvolt = <5000000>;
	};

	vcc_wifi: vcc-wifi {
		status = "okay";
		compatible = "regulator-fixed";
		regulator-name = "vcc_wifi";
		enable-active-high;
		gpio = <&gpio4 27 GPIO_ACTIVE_HIGH>;
		pinctrl-names = "default";
		pinctrl-0 = <&vcc_wifi_h>;
		regulator-always-on;
		regulator-boot-on;
		regulator-min-microvolt = <5000000>;
		regulator-max-microvolt = <5000000>;
	};
	vcc_pcie: vcc-pcie {
		status = "okay";
		compatible = "regulator-fixed";
		regulator-name = "vcc_pcie";
		enable-active-high;
		gpio = <&gpio1 17 GPIO_ACTIVE_HIGH>;
		pinctrl-names = "default";
		pinctrl-0 = <&vcc_pcie_h>;
		regulator-always-on;
		regulator-boot-on;
		regulator-min-microvolt = <5000000>;
		regulator-max-microvolt = <5000000>;
	};

	vcc_chargen: vcc-chargen {
		status = "disabled";
		compatible = "regulator-fixed";
		regulator-name = "vcc_chargen";
		enable-active-high;
		gpio = <&gpio2 28 GPIO_ACTIVE_HIGH>;
		pinctrl-names = "default";
		pinctrl-0 = <&vcc_chargen_h>;
		regulator-always-on;
		regulator-boot-on;
		regulator-min-microvolt = <5000000>;
		regulator-max-microvolt = <5000000>;
	};

	vcc_phy: vcc-phy-regulator {
		compatible = "regulator-fixed";
		regulator-name = "vcc_phy";
		regulator-always-on;
		regulator-boot-on;
	};

	vdd_log: vdd-log {
		compatible = "pwm-regulator";
		pwms = <&pwm2 0 25000 1>;
		regulator-name = "vdd_log";
		regulator-min-microvolt = <800000>;
		regulator-max-microvolt = <1100000>;
		regulator-always-on;
		regulator-boot-on;

		/* for rockchip boot on */
		rockchip,pwm_id= <2>;
		rockchip,pwm_voltage = <1000000>;
	};

	usb-charge {
		compatible = "usb-ext-charge";
		status = "okay";
		io-channels = <&saradc 0>;
        extcon = <&fusb0>;
		pinctrl-names = "default";
		pinctrl-0 = <&vcc_chargen_h &bat_int_h &cur_ctl_h &poe_det_h>;
	    bat-int = <&gpio2 28 IRQ_TYPE_EDGE_RISING>;
		charge-en-gpios = <&gpio0 1 GPIO_ACTIVE_HIGH>;
		cur-ctl-gpios = <&gpio3 26 GPIO_ACTIVE_HIGH>;
		poe-state-gpios = <&gpio0 12 GPIO_ACTIVE_HIGH>;
	};
};

&cpu_l0 {
	cpu-supply = <&vdd_cpu_l>;
};

&cpu_l1 {
	cpu-supply = <&vdd_cpu_l>;
};

&cpu_l2 {
	cpu-supply = <&vdd_cpu_l>;
};

&cpu_l3 {
	cpu-supply = <&vdd_cpu_l>;
};

&cpu_b0 {
	cpu-supply = <&vdd_cpu_b>;
};

&cpu_b1 {
	cpu-supply = <&vdd_cpu_b>;
};

&emmc_phy {
	status = "okay";
};

&gmac {
	phy-supply = <&vcc_phy>;
	phy-mode = "rgmii";
	clock_in_out = "input";
	snps,reset-gpio = <&gpio3 15 GPIO_ACTIVE_LOW>;
	snps,reset-active-low;
	snps,reset-delays-us = <0 10000 50000>;
	assigned-clocks = <&cru SCLK_RMII_SRC>;
	assigned-clock-parents = <&clkin_gmac>;
	pinctrl-names = "default";
	pinctrl-0 = <&rgmii_pins>;
	tx_delay = <0x28>;
	rx_delay = <0x1B>;
	status = "okay";
};

&gpu {
	status = "okay";
	mali-supply = <&vdd_gpu>;
};

&i2c0 {
	status = "okay";
	i2c-scl-rising-time-ns = <168>;
	i2c-scl-falling-time-ns = <4>;
	clock-frequency = <400000>;

	vdd_cpu_b: syr827@40 {
		compatible = "silergy,syr827";
		reg = <0x40>;
		vin-supply = <&vcc5v0_sys>;
		regulator-compatible = "fan53555-reg";
		regulator-name = "vdd_cpu_b";
		pinctrl-0 = <&vsel1_gpio>;
		vsel-gpios = <&gpio1 18 GPIO_ACTIVE_HIGH>;
		regulator-min-microvolt = <712500>;
		regulator-max-microvolt = <1500000>;
		regulator-ramp-delay = <1000>;
		fcs,suspend-voltage-selector = <1>;
		regulator-always-on;
		regulator-boot-on;
		regulator-initial-state = <3>;
			regulator-state-mem {
			regulator-off-in-suspend;
		};
	};

	vdd_gpu: syr828@41 {
		compatible = "silergy,syr828";
		reg = <0x41>;
		vin-supply = <&vcc5v0_sys>;
		regulator-compatible = "fan53555-reg";
		regulator-name = "vdd_gpu";
		pinctrl-0 = <&vsel2_gpio>;
		vsel-gpios = <&gpio1 14 0>;
		regulator-min-microvolt = <712500>;
		regulator-max-microvolt = <1500000>;
		regulator-ramp-delay = <1000>;
		fcs,suspend-voltage-selector = <1>;
		regulator-always-on;
		regulator-boot-on;
		regulator-initial-state = <3>;
			regulator-state-mem {
			regulator-off-in-suspend;
		};
	};

	rk808: pmic@1b {
		compatible = "rockchip,rk808";
		reg = <0x1b>;
		interrupt-parent = <&gpio1>;
		interrupts = <21 IRQ_TYPE_LEVEL_LOW>;
		pinctrl-names = "default";
		//pinctrl-0 = <&pmic_int_l &pmic_dvs2>;
		pinctrl-0 = <&pmic_int_l &pmic_dvs2 &vcc_5v0_h>;
		rockchip,system-power-controller;
		wakeup-source;
		#clock-cells = <1>;
		clock-output-names = "xin32k", "rk808-clkout2";

		pmic,hold-gpio = <&gpio2 6 GPIO_ACTIVE_HIGH>;

		//vin-supply = <&sys_12v>;

		vcc1-supply = <&vcc3v3_sys>;
		vcc2-supply = <&vcc3v3_sys>;
		vcc3-supply = <&vcc3v3_sys>;
		vcc4-supply = <&vcc3v3_sys>;
		vcc6-supply = <&vcc3v3_sys>;
		vcc7-supply = <&vcc3v3_sys>;
		vcc8-supply = <&vcc3v3_sys>;
		vcc9-supply = <&vcc3v3_sys>;
		vcc10-supply = <&vcc3v3_sys>;
		vcc11-supply = <&vcc3v3_sys>;
		vcc12-supply = <&vcc3v3_sys>;
		vddio-supply = <&vcc1v8_pmu>;

		regulators {
			vdd_center: DCDC_REG1 {
				regulator-always-on;
				regulator-boot-on;
				regulator-min-microvolt = <750000>;
				regulator-max-microvolt = <1350000>;
				regulator-ramp-delay = <6001>;
				regulator-name = "vdd_center";
				regulator-state-mem {
					regulator-off-in-suspend;
				};
			};

			vdd_cpu_l: DCDC_REG2 {
				regulator-always-on;
				regulator-boot-on;
				regulator-min-microvolt = <750000>;
				regulator-max-microvolt = <1350000>;
				regulator-ramp-delay = <6001>;
				regulator-name = "vdd_cpu_l";
				regulator-state-mem {
					regulator-off-in-suspend;
				};
			};

			vcc_ddr: DCDC_REG3 {
				regulator-always-on;
				regulator-boot-on;
				regulator-name = "vcc_ddr";
				regulator-state-mem {
					regulator-on-in-suspend;
				};
			};

			vcc_1v8: DCDC_REG4 {
				regulator-always-on;
				regulator-boot-on;
				regulator-min-microvolt = <1800000>;
				regulator-max-microvolt = <1800000>;
				regulator-name = "vcc_1v8";
				regulator-state-mem {
					regulator-on-in-suspend;
					regulator-suspend-microvolt = <1800000>;
				};
			};

			vcca1v8_codec: LDO_REG1 {
				regulator-always-on;
				regulator-boot-on;
				regulator-min-microvolt = <1800000>;
				regulator-max-microvolt = <1800000>;
				regulator-name = "vcca1v8_codec";
				regulator-state-mem {
					regulator-off-in-suspend;
				};
			};

			vcca1v8_hdmi: LDO_REG2 {
				regulator-always-on;
				regulator-boot-on;
				regulator-min-microvolt = <1800000>;
				regulator-max-microvolt = <1800000>;
				regulator-name = "vcca1v8_hdmi";
				regulator-state-mem {
					regulator-off-in-suspend;
				};
			};

			vcc1v8_pmu: LDO_REG3 {
				regulator-always-on;
				regulator-boot-on;
				regulator-min-microvolt = <1800000>;
				regulator-max-microvolt = <1800000>;
				regulator-name = "vcc1v8_pmu";
				regulator-state-mem {
					regulator-on-in-suspend;
					regulator-suspend-microvolt = <1800000>;
				};
			};

			vccio_sd: LDO_REG4 {
				regulator-always-on;
				regulator-boot-on;
				regulator-min-microvolt = <1800000>;
				regulator-max-microvolt = <3000000>;
				regulator-name = "vccio_sd";
				regulator-state-mem {
					regulator-on-in-suspend;
					regulator-suspend-microvolt = <3000000>;
				};
			};

			vcca3v0_codec: LDO_REG5 {
				regulator-always-on;
				regulator-boot-on;
				regulator-min-microvolt = <3000000>;
				regulator-max-microvolt = <3000000>;
				regulator-name = "vcca3v0_codec";
				regulator-state-mem {
					regulator-off-in-suspend;
				};
			};

			vcc_1v5: LDO_REG6 {
				regulator-always-on;
				regulator-boot-on;
				regulator-min-microvolt = <1500000>;
				regulator-max-microvolt = <1500000>;
				regulator-name = "vcc_1v5";
				regulator-state-mem {
					regulator-on-in-suspend;
					regulator-suspend-microvolt = <1500000>;
				};
			};

			vcc0v9i_hdmi: LDO_REG7 {
				regulator-always-on;
				regulator-boot-on;
				regulator-min-microvolt = <900000>;
				regulator-max-microvolt = <900000>;
				regulator-name = "vcc0v9i_hdmi";
				regulator-state-mem {
					regulator-off-in-suspend;
				};
			};

			vcc_3v0: LDO_REG8 {
				regulator-always-on;
				regulator-boot-on;
				regulator-min-microvolt = <3000000>;
				regulator-max-microvolt = <3000000>;
				regulator-name = "vcc_3v0";
				regulator-state-mem {
					regulator-on-in-suspend;
					regulator-suspend-microvolt = <3000000>;
				};
			};

			vcc3v3_s3: SWITCH_REG1 {
				regulator-always-on;
				regulator-boot-on;
				regulator-name = "vcc3v3_s3";
				regulator-state-mem {
					regulator-off-in-suspend;
				};
			};

			vcc3v3_s0: SWITCH_REG2 {
				regulator-always-on;
				regulator-boot-on;
				regulator-name = "vcc3v3_s0";
				regulator-state-mem {
					regulator-off-in-suspend;
				};
			};
		};
	};
};


&i2c1 {
	status = "okay";
	//i2c-scl-rising-time-ns = <300>;
	//i2c-scl-falling-time-ns = <15>;
};

&i2c3 {                                                                                                                                        
        status = "disabled";
        i2c-scl-rising-time-ns = <450>;
        i2c-scl-falling-time-ns = <15>;
};

&i2c4 {
	status = "okay";
	i2c-scl-rising-time-ns = <475>;
	i2c-scl-falling-time-ns = <26>;

	fusb1: fusb30x@22 {
		compatible = "fairchild,fusb302";
		reg = <0x22>;
		pinctrl-names = "default";
		pinctrl-0 = <&fusb1_int &fusb1_vbus_drv>;
		int-n-gpios = <&gpio1 1 GPIO_ACTIVE_HIGH>;
		vbus-5v-gpios = <&gpio1 13 GPIO_ACTIVE_HIGH>;
		//vbus-supply = <&sys_12v>;
		status = "okay";
	};
};

&i2c7 {
	status = "okay";
	i2c-scl-rising-time-ns = <345>;
	i2c-scl-falling-time-ns = <11>;
	clock-frequency = <400000>;

	mp8859: mp8859@66 {                                           

		compatible = "mps,mp8859";
		reg = <0x66>;
		status = "okay";
		mp,max-input-voltage = <15000000>;
		mp,max-input-current = <3000000>;

		regulators {
			sys_12v: mp8859_dcdc1 {
				regulator-name = "sys_12v";
				regulator-min-microvolt = <12000000>;
				regulator-max-microvolt = <12000000>;
				regulator-ramp-delay = <8000>;
				regulator-always-on;
				regulator-boot-on;
			};
		};
	};

	fusb0: fusb30x@22 {
		compatible = "fairchild,fusb302";
		reg = <0x22>;
		charge-dev = <&mp8859>;
		pinctrl-names = "default";
		pinctrl-0 = <&fusb0_int>;
		int-n-gpios = <&gpio1 2 GPIO_ACTIVE_HIGH>;
		status = "okay";
	};
};


&i2s0 {
	status = "okay";
	rockchip,i2s-broken-burst-len;
	rockchip,playback-channels = <8>;
	rockchip,capture-channels = <8>;
	#sound-dai-cells = <0>;
};

&i2s1 {
	status = "okay";
	rockchip,i2s-broken-burst-len;
	rockchip,playback-channels = <2>;
	rockchip,capture-channels = <2>;
	#sound-dai-cells = <0>;
	assigned-clocks = <&cru SCLK_I2S1_DIV>, <&cru SCLK_I2S_8CH>;
	assigned-clock-parents = <&cru PLL_GPLL>, <&cru SCLK_I2S1_8CH>;
};

&i2s2 {
	#sound-dai-cells = <0>;
	dmas = <&dmac_bus 4>;
	dma-names = "tx";
	status = "okay";
	assigned-clocks = <&cru SCLK_I2S2_DIV>, <&cru SCLK_I2S_8CH>;
	assigned-clock-parents = <&cru PLL_GPLL>, <&cru SCLK_I2S2_8CH>;
};

&io_domains {
	status = "okay";
	bt656-supply = <&vcc_3v0>;		/* bt656_gpio2ab_ms */
	audio-supply = <&vcca1v8_codec>;	/* audio_gpio3d4a_ms */
	sdmmc-supply = <&vccio_sd>;		/* sdmmc_gpio4b_ms */
	gpio1830-supply = <&vcc_3v0>;		/* gpio1833_gpio4cd_ms */
};

&pcie_phy {
	status = "okay";
};

&pmu_io_domains {
	status = "okay";
	pmu1830-supply = <&vcc_3v0>;
};

&pwm0 {
	status = "okay";
};

&pwm1 {
	status = "okay";
};

&pwm2 {
	status = "okay";
};

&sdhci {
	bus-width = <8>;
	mmc-hs400-1_8v;
	supports-emmc;
	non-removable;
	keep-power-in-suspend;
	mmc-hs400-enhanced-strobe;
	status = "okay";
};

&sdmmc {
	clock-frequency = <150000000>;
	clock-freq-min-max = <100000 150000000>;
	supports-sd;
	bus-width = <4>;
	cap-mmc-highspeed;
	cap-sd-highspeed;
	disable-wp;
	num-slots = <1>;
	sd-uhs-sdr104;
	vmmc-supply = <&vcc_sd>;
	vqmmc-supply = <&vccio_sd>;
	pinctrl-names = "default";
	pinctrl-0 = <&sdmmc_clk &sdmmc_cmd &sdmmc_cd &sdmmc_bus4>;
	status = "okay";
};

&uart0 {
	pinctrl-names = "default";
	pinctrl-0 = <&uart0_xfer &uart0_cts>;
	status = "okay";
};

&rockchip_suspend {
	status = "okay";
	rockchip,sleep-debug-en = <1>;
	rockchip,sleep-mode-config = <
		(0
		| RKPM_SLP_ARMPD
		| RKPM_SLP_PERILPPD
		| RKPM_SLP_DDR_RET
		| RKPM_SLP_PLLPD
		| RKPM_SLP_CENTER_PD
		| RKPM_SLP_AP_PWROFF
		)
		>;
	rockchip,wakeup-config = <
		(0
		| RKPM_GPIO_WKUP_EN
		| RKPM_PWM_WKUP_EN
		)
		>;
		rockchip,pwm-regulator-config = <
		(0
		| PWM2_REGULATOR_EN
		)
		>;
		rockchip,power-ctrl =
		<&gpio1 17 GPIO_ACTIVE_HIGH>,
		<&gpio1 14 GPIO_ACTIVE_HIGH>;
};



&saradc {
	status = "okay";
};

&tcphy0 {
	extcon = <&fusb0>;
	status = "okay";
};

&tcphy1 {
	extcon = <&fusb1>;
	status = "okay";
};

&tsadc {
	/* tshut mode 0:CRU 1:GPIO */
	rockchip,hw-tshut-mode = <1>;
	/* tshut polarity 0:LOW 1:HIGH */
	rockchip,hw-tshut-polarity = <1>;
	status = "okay";
};

&u2phy0 {
	status = "okay";
	extcon = <&fusb0>;

	u2phy0_otg: otg-port {
		status = "okay";
	};

	u2phy0_host: host-port {
		phy-supply = <&vcc5v0_host>;
		status = "okay";
	};
};

&u2phy1 {
	status = "okay";
	extcon = <&fusb1>;

	u2phy1_otg: otg-port {
		status = "okay";
	};

	u2phy1_host: host-port {
		phy-supply = <&vcc5v0_host>;
		status = "okay";
	};
};

&uart2 {
	status = "okay";
};

&usbdrd3_0 {
	status = "okay";
	extcon = <&fusb0>;
};

&usbdrd3_1 {
	status = "okay";
	extcon = <&fusb1>;
};

&usbdrd_dwc3_0 {
	status = "okay";
};

&usbdrd_dwc3_1 {
	status = "okay";
	dr_mode = "host";  //only host
};

&usb_host0_ehci {
	status = "okay";
};

&usb_host0_ohci {
	status = "okay";
};

&usb_host1_ehci {
	status = "okay";
};

&usb_host1_ohci {
	status = "okay";
};

&pinctrl {
	edp {
		lcd_panel_enable: lcd-panel-enable {
			rockchip,pins = <2 5 RK_FUNC_GPIO &pcfg_pull_up>;
		};
		lcd_backlight_enable: lcd-backlight-enable {
			rockchip,pins = <2 24 RK_FUNC_GPIO &pcfg_pull_up>;
		};
		lcd_edp_hpd_enable: lcd-edp-hpd-enable{
			rockchip,pins = <1 20 RK_FUNC_GPIO &pcfg_pull_up>;
		};
	};

	pmic {
		pmic_int_l: pmic-int-l {
			rockchip,pins =
				<1 21 RK_FUNC_GPIO &pcfg_pull_up>;
		};

		pmic_dvs2: pmic-dvs2 {
			rockchip,pins =
				<1 18 RK_FUNC_GPIO &pcfg_pull_down>;
		};

		vsel1_gpio: vsel1-gpio {
				rockchip,pins =
					<1 18 0 &pcfg_pull_down>;
		};

		vsel2_gpio: vsel2-gpio {
				rockchip,pins =
					<1 14 0 &pcfg_pull_down>;
		};
	};

	usb2 {
		host_vbus_drv: host-vbus-drv {
			rockchip,pins =
				<1 0 RK_FUNC_GPIO &pcfg_pull_none>;
		};

		hub_rst_en: hub-rst-en {
			rockchip,pins =
				<2 4 RK_FUNC_GPIO &pcfg_pull_none>;
		};
	};

	fusb30x {
		fusb0_int: fusb0-int {
			rockchip,pins = <1 2 RK_FUNC_GPIO &pcfg_pull_up>;
		};

		fusb1_int: fusb1-int {
			rockchip,pins = <1 1 RK_FUNC_GPIO &pcfg_pull_up>;
		};

		fusb1_vbus_drv: fusb1-vbus-drv{
			rockchip,pins = <1 13 RK_FUNC_GPIO &pcfg_pull_up>;
		};
	};

	leds {
		led_power: led-power {
			rockchip,pins = <2 27 RK_FUNC_GPIO &pcfg_pull_none>;
		};

		led_user: led-user {
			rockchip,pins = <0 13 RK_FUNC_GPIO &pcfg_pull_none>;
		};
	};

	vcc5v0_sys {
		vcc_5v0_h: vcc-5v0-h {
			rockchip,pins = <2 6 RK_FUNC_GPIO &pcfg_pull_none>;
		};
	};
	vcc_sd {
		vcc_sd_h: vcc-sd-h {
			rockchip,pins = <4 30 RK_FUNC_GPIO &pcfg_pull_none>;
		};
	};

	wifi {
		vcc_wifi_h: vcc-wifi-h {
			rockchip,pins = <4 27 RK_FUNC_GPIO &pcfg_pull_none>;
		};
	};

	pcie {
		vcc_pcie_h: vcc-pcie-h {
			rockchip,pins = <1 17 RK_FUNC_GPIO &pcfg_pull_none>;
		};
	};

	chargen {
		vcc_chargen_h: vcc-chargen-h {
			rockchip,pins = <0 1 RK_FUNC_GPIO &pcfg_pull_none>;
		};
		bat_int_h: bat-int-h {
			rockchip,pins = <2 28 RK_FUNC_GPIO &pcfg_pull_none>;
		};
		cur_ctl_h: cur-ctl-h {
			rockchip,pins = <3 26 RK_FUNC_GPIO &pcfg_pull_none_20ma>;
		};
		poe_det_h: poe-det-h {
			rockchip,pins = <0 12 RK_FUNC_GPIO &pcfg_pull_down>;
		};
	};
};

&spi1 {
	status = "okay";
	max-freq = <48000000>;
	dev-port = <0>;

	spidev0: spidev@00 {
		status = "okay";
		//compatible = "linux,spidev";
		compatible = "jedec,spi-nor";
		reg = <0x00>;
		spi-max-frequency = <48000000>;
	};
};

&pwm3 {
	status = "okay";
	interrupts = <GIC_SPI 61 IRQ_TYPE_LEVEL_HIGH 0>;
	compatible = "rockchip,remotectl-pwm";
	remote_pwm_id = <3>;
	handle_cpu_id = <0>;
	remote_support_psci = <1>;

    ir_key1{
        rockchip,usercode = <0xff00>;
        rockchip,key_table =
            <0xeb   KEY_POWER>,
            <0xec   KEY_MENU>,
            <0xfe   KEY_BACK>,
            <0xb7   KEY_HOME>,
            <0xa3   KEY_WWW>,
            <0xf4   KEY_VOLUMEUP>,
            <0xa7   KEY_VOLUMEDOWN>,
            <0xf8   KEY_REPLY>,
            <0xfc   KEY_UP>,
            <0xfd   KEY_DOWN>,
            <0xf1   KEY_LEFT>,
            <0xe5   KEY_RIGHT>;
    };
};

&uart4 {
	current-speed = <9600>;
	no-loopback-test;
	status = "disabled";
};

&isp0 {
        status = "okay";
        rockchip,gpios = <&gpio1 22 GPIO_ACTIVE_HIGH>;
};

&isp1 {
        status = "okay";
        rockchip,gpios = <&gpio1 22 GPIO_ACTIVE_HIGH>;
};

&isp0_mmu {
    status = "okay";
};

&isp1_mmu {
    status = "okay";
};

&edp {
	status = "okay";

	ports {
		edp_out: port@1 {
			reg = <1>;
			#address-cells = <1>;
			#size-cells = <0>;

			edp_out_panel: endpoint@0 {
				reg = <0>;
				remote-endpoint = <&panel_in_edp>;
			};
		};
	};
};

&edp_in_vopb {
	status = "okay";
};

&edp_in_vopl {
	status = "disabled";
};

&hdmi_in_vopb {
	status = "disabled";
};

&hdmi_in_vopl {
	status = "okay";
};

&dp_in_vopb {
        status = "disabled";
};

&dp_in_vopl {
        status = "okay";
};

&hdmi_dp_sound {
	status = "okay";
};

&route_hdmi {
	status = "okay";
	logo,mode = "center";
//	video,hdisplay = <1024>;
//	video,vdisplay = <768>;
//	video,vrefresh = <60>;
//	video,flags = <(DRM_MODE_FLAG_NHSYNC | DRM_MODE_FLAG_NVSYNC)>;
};

&route_edp {
	status = "okay";
	logo,mode = "center";
};

&cdn_dp {
		status = "okay";
        extcon = <&fusb0 &fusb1>;
        phys = <&tcphy0_dp &tcphy1_dp>;
        //extcon = <&fusb1>;
        //phys = <&tcphy1_dp>;
};

&wdt {
    status = "disabled";
};

&dfi {
	status = "okay";
};

&dmc {
	status = "okay";
	center-supply = <&vdd_center>;
	system-status-freq = <
		/*system status         freq(KHz)*/
		SYS_STATUS_NORMAL       800000
		SYS_STATUS_REBOOT       400000
		SYS_STATUS_SUSPEND      400000
		SYS_STATUS_VIDEO_1080P  400000
		SYS_STATUS_VIDEO_4K     800000
		SYS_STATUS_VIDEO_4K_10B 800000
		SYS_STATUS_PERFORMANCE  800000
		SYS_STATUS_BOOST        400000
		SYS_STATUS_DUALVIEW     800000
		SYS_STATUS_ISP          800000
	>;
	auto-min-freq = <400000>;
	auto-freq-en = <0>;
};

&dmc_opp_table {
	compatible = "operating-points-v2";

	opp-200000000 {
		opp-hz = /bits/ 64 <200000000>;
		opp-microvolt = <825000>;
		status = "disabled";
	};
	opp-300000000 {
		opp-hz = /bits/ 64 <300000000>;
		opp-microvolt = <850000>;
		status = "disabled";
	};
	opp-400000000 {
		opp-hz = /bits/ 64 <400000000>;
		opp-microvolt = <900000>;
	};
	opp-528000000 {
		opp-hz = /bits/ 64 <528000000>;
		opp-microvolt = <900000>;
		status = "disabled";
	};
	opp-600000000 {
		opp-hz = /bits/ 64 <600000000>;
		opp-microvolt = <900000>;
		status = "disabled";
	};
	opp-800000000 {
		opp-hz = /bits/ 64 <800000000>;
		opp-microvolt = <900000>;
	};
	opp-928000000 {
		opp-hz = /bits/ 64 <928000000>;
		opp-microvolt = <900000>;
		status = "disabled";
	};
	opp-1056000000 {
		opp-hz = /bits/ 64 <1056000000>;
		opp-microvolt = <900000>;
		status = "disabled";
	};
};


