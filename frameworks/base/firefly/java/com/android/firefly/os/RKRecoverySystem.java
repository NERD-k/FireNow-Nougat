/*************************************************************************
	> File Name: RKRecoverySystem.java
	> Author: jkand.huang
	> Mail: jkand.huang@rock-chips.com
	> Created Time: Wed 02 Nov 2016 03:10:47 PM CST
 ************************************************************************/
package android.firefly.os; 
import android.os.RecoverySystem;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.ConditionVariable;
import android.os.PowerManager;
import android.os.FileUtils;
import android.os.storage.StorageManager;
import android.os.storage.VolumeInfo;
import android.os.storage.DiskInfo;
import android.os.Environment;
import android.util.Log;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;

import java.util.Collection;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.lang.reflect.*;




public class RKRecoverySystem extends RecoverySystem{
	private static final String TAG = "RKRecoverySystem";
	private static File RECOVERY_DIR = new File("/cache/recovery");
	private static File UPDATE_FLAG_FILE = new File(RECOVERY_DIR, "last_flag");
	private static File ANDROID_REAL_PATH_FILE = new File(RECOVERY_DIR, "ota_android_real_path");


    	public static String DATA_ROOT = "/data/media/0";
    	public static String FLASH_ROOT = Environment.getExternalStorageDirectory().getAbsolutePath();
    	public static String SDCARD_ROOT = "/mnt/external_sd";
    	public static String USB_ROOT = "/mnt/usb_storage";
    	public static String CACHE_ROOT = Environment.getDownloadCacheDirectory().getAbsolutePath();

        public static String OTA_PACKAGE_FILE = "update.zip";
	public static String RKIMAGE_FILE = "update.img";	

	public static void installPackage(Context context, File packageFile)throws IOException{
		String filename = packageFile.getCanonicalPath();
		writeRealPath(filename);
		writeFlagCommand(filename);

		int SDK_VERSION = android.os.Build.VERSION.SDK_INT;
		StorageManager storageManager = (StorageManager) context.getSystemService(Context.STORAGE_SERVICE);

		if (SDK_VERSION >= 23 && !filename.startsWith(DATA_ROOT)) {
			String imagePath = filename;
			List<VolumeInfo> volumes = storageManager.getVolumes();
			for (VolumeInfo vol : volumes) {
				File file = vol.getPath();

				if (vol.getType() == VolumeInfo.TYPE_PUBLIC && 
						file != null && filename.startsWith(file.getCanonicalPath())) {

					DiskInfo disk = vol.getDisk();
					if (disk != null && disk.isUsb()) {
						imagePath = USB_ROOT + "/" + OTA_PACKAGE_FILE;
						break;
					} else if (disk != null && disk.isSd()) {
						imagePath = SDCARD_ROOT + "/" + OTA_PACKAGE_FILE;
						break;
					}
				}
			}

			packageFile = new File(imagePath);
		}

               RecoverySystem.installPackage(context, packageFile);
	}

	public static String readFlagCommand() {
		return readFile(UPDATE_FLAG_FILE);
	}

	public static String readRealPath() {
		return readFile(ANDROID_REAL_PATH_FILE);
	}

	private static String readFile(File file) {
		if(file.exists()) {
			char[] buf = new char[128];
			int readCount = 0;
			String path = null;
			try {
				path = file.getCanonicalPath();
				Log.d(TAG, path + " is exists");
				FileReader reader = new FileReader(file);
				readCount = reader.read(buf, 0, buf.length);
				Log.d(TAG, "readCount = " + readCount + " buf.length = " + buf.length);
			}catch (IOException e) {
				Log.e(TAG, "can not read " + path);
			}finally {
				file.delete();
				
			}
			
			StringBuilder sBuilder = new StringBuilder();
			for(int i = 0; i < readCount; i++) {
				if(buf[i] == 0) {
					break;
				}
				sBuilder.append(buf[i]);
			}
			return sBuilder.toString();
		}else {
			return null;
		}
	}
	
	public static void writeFlagCommand(String path) throws IOException{
		writeFile(UPDATE_FLAG_FILE, "updating$path=" + path);
	}

	public static void writeRealPath(String path) throws IOException {
		writeFile(ANDROID_REAL_PATH_FILE, path);
	}

	private static void writeFile(File file, String content) throws IOException {
		file.delete();
		RECOVERY_DIR.mkdirs();
		FileWriter writer = new FileWriter(file);
		try {
			writer.write(content);
		}finally {
			writer.close();
		}
	}
}
